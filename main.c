#include <stdio.h>
#include "src/stack.h"

#define DEBUG_MAIN
#ifdef DEBUG_MAIN
    #define __MDEBUG(fmt, args...)    fprintf(stderr, "[main] "fmt, ## args)
#else
    #define __MDEBUG(fmt, args...)    /* Don't do anything in release builds */
#endif

int main(int argc, char *argv[]){
    STACK_TTYPE temp;
    int i;
    int ret;
    Stack stack1, stack2;
    __MDEBUG("init stack\n");
    ret = stack_init(&stack1);
    if(-1 == ret){
        return -1;
    }
    ret = stack_init(&stack2);
    if(-1 == ret){
        return -1;
    }
    printf("\n");
    for(i = 0; i <= 10*STACK_MAX; i++){
        temp.first=i;
        temp.second=i*2;
        ret = stack_push(&stack1, temp);
        if(-1 == ret){
            __MDEBUG("push i=%d item='reject' ret=%d\n", i, ret);
            stack_do_all(&stack1, &stack_print_item, NULL);
            printf("\n");
            stack_rdo_all(&stack1, &stack_print_item, NULL);
            printf("\n");
            //break;
        }
        else{
            __MDEBUG("push i=%d item=(%d,%d) ret=%d\n", i, temp.first, temp.second, ret);
            stack_do_all(&stack1, &stack_print_item, NULL);
            printf("\n");
            stack_rdo_all(&stack1, &stack_print_item, NULL);
            printf("\n");
        }
        temp.first=i*2;
        temp.second=i*3;
        ret = stack_push(&stack2, temp);
        if(-1 == ret){
            __MDEBUG("push i=%d item='reject' ret=%d\n", i, ret);
            stack_do_all(&stack2, &stack_print_item, NULL);
            printf("\n");
            stack_rdo_all(&stack2, &stack_print_item, NULL);
            printf("\n");
            //break;
        }
        else{
            __MDEBUG("push i=%d item=(%d,%d) ret=%d\n", i, temp.first, temp.second, ret);
            stack_do_all(&stack2, &stack_print_item, NULL);
            printf("\n");
            stack_rdo_all(&stack2, &stack_print_item, NULL);
            printf("\n");
        }
        printf("\n");
    }
    printf("\n");
    printf("\n");
    for(i = 0; i<= 2*STACK_MAX; i++){
        ret = stack_pop(&stack1, &temp);
        if(STACK_NOITEM == ret){
            __MDEBUG("pop i=%d item='empty' ret=%d\n", i, ret);
            stack_do_all(&stack1, &stack_print_item, NULL);
            printf("\n");
            stack_rdo_all(&stack1, &stack_print_item, NULL);
            printf("\n");
        }
        else{
            __MDEBUG("pop i=%d item=(%d,%d) ret=%d\n", i, temp.first, temp.second, ret);
            stack_do_all(&stack1, &stack_print_item, NULL);
            printf("\n");
            stack_rdo_all(&stack1, &stack_print_item, NULL);
            printf("\n");
        }
        ret = stack_pop(&stack2, &temp);
        if(STACK_NOITEM == ret){
            __MDEBUG("pop i=%d item='empty' ret=%d\n", i, ret);
            stack_do_all(&stack2, &stack_print_item, NULL);
            printf("\n");
            stack_rdo_all(&stack2, &stack_print_item, NULL);
            printf("\n");
            break;
        }
        else{
            __MDEBUG("pop i=%d item=(%d,%d) ret=%d\n", i, temp.first, temp.second, ret);
            stack_do_all(&stack2, &stack_print_item, NULL);
            printf("\n");
            stack_rdo_all(&stack2, &stack_print_item, NULL);
            printf("\n");
        }
        printf("\n");
    }
    stack_free(&stack1);
    stack_free(&stack2);
    return 0;
}

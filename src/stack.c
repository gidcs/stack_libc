#include "stack.h"

int stack_init(Stack *stack){
    int ret;
    
    stack->min = STACK_NOITEM; //0
    stack->max = STACK_MAX;
    stack->top = stack->min; //the last item
    
#ifdef STACK_ALLOC_FAIL_SIM
    stack->alloc_cnt = 0;
#endif

    ret = stack_alloc(stack);
    if(-1 == ret){
        __SDEBUG("%s(%p) alloc failed\n", __func__, stack);
        return -1;
    }

    __SDEBUG("%s(%p) top=%d\n", __func__, stack, stack->top);
    return 0;
}

int stack_size(Stack *stack){
    return stack->max+1;
}

int stack_top(Stack *stack){
    __SDEBUG("%s(%p) top=%d\n", __func__, stack, stack->top);
    return stack->top;
}

int stack_alloc(Stack *stack){
    stack->ptr = NULL;

#ifdef STACK_ALLOC_FAIL_SIM
    stack->alloc_cnt++;
    if(STACK_ALLOC_FAIL_CNT == stack->alloc_cnt){
        __SDEBUG("%s(%p) alloc(%d items) failed (fake fail)\n", __func__, stack, stack->max);
        return -1;
    }
#endif

    stack->ptr = malloc(sizeof(STACK_TTYPE)*stack_size(stack));
    if(stack == NULL){
        __SDEBUG("%s(%p) alloc(%d items) failed\n", __func__, stack, stack->max);
        return -1;
    }
    __SDEBUG("%s(%p) alloc(%d items) ok ptr(%p)\n", __func__, stack, stack->max, stack->ptr);
    return 0;
}

int stack_realloc(Stack *stack){
    STACK_TTYPE *tmp_ptr;
    int tmp_size;
    int ret;

    tmp_ptr = stack->ptr;
    tmp_size = stack_size(stack);
    stack->max = stack->max << 1;
    ret = stack_alloc(stack);
    if(-1 == ret){
        stack->max = stack->max >> 1;
        stack->ptr = tmp_ptr;
        __SDEBUG("%s(%p) failed\n", __func__, stack);
        return -1;
    }
    
    __SDEBUG("%s(%p) new_ptr=%p old_ptr=%p\n", __func__, stack, stack->ptr, tmp_ptr);
    memcpy(stack->ptr, tmp_ptr, sizeof(STACK_TTYPE)*tmp_size);
    free(tmp_ptr);
    __SDEBUG("%s(%p) memcpy(items+1=%d) done\n", __func__, stack, tmp_size);
    return 0;
}

void stack_free(Stack *stack){
    free(stack->ptr);
    stack->top = stack->min;
    __SDEBUG("%s(%p) top=%d\n", __func__, stack, stack->top);
}

int stack_push(Stack *stack, STACK_TTYPE item){
    int ret;

    if(stack->max == stack->top){
        ret = stack_realloc(stack);
        if(-1 == ret){
            __SDEBUG("%s(%p) full\n", __func__, stack);
            return -1;
        }
    }
    
    stack->top++;
    memcpy(&(stack->ptr[stack->top]), &item, sizeof(STACK_TTYPE));
    __SDEBUG("%s(%p) top=%d\n", __func__, stack, stack->top);
    return stack->top;
}

int stack_pop(Stack *stack, STACK_TTYPE *item){
    if(stack->min == stack->top){
        __SDEBUG("%s(%p) empty\n", __func__, stack);
        return STACK_NOITEM;
    }
    
    stack->top--;
    __SDEBUG("%s(%p) top=%d\n", __func__, stack, stack->top);

    memcpy(item, &(stack->ptr[stack->top+1]), sizeof(STACK_TTYPE));
    return stack->top+1;
}

void stack_do_all(Stack *stack, int (*fptr)(STACK_TTYPE *, void *), void *params){
    int index;
    int break_flag;

    __SDEBUG("%s(%p) list: ", __func__, stack);

    index = stack->min + 1; /* index of first item is 1 */
    while(index <= stack->top){
        break_flag = (*fptr)(&(stack->ptr[index]), params);
        if(1 == break_flag) break;
        index++;
    }
}

void stack_rdo_all(Stack *stack, int (*fptr)(STACK_TTYPE *, void *), void *params){
    int index;
    int break_flag;

    __SDEBUG("%s(%p) list: ", __func__, stack);

    index = stack->top;
    while(index > stack->min){ /* min=0, index of first item is 1 */
        break_flag = (*fptr)(&(stack->ptr[index]), params);
        if(1 == break_flag) break;
        index--;
    }
}


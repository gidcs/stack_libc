#ifndef STACK_HEADER
#define STACK_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack_type.h"

#define DEBUG_STACK
#ifdef DEBUG_STACK
    #define __SDEBUG(fmt, args...)    fprintf(stderr, "    [stack] "fmt, ## args)
#else
    #define __SDEBUG(fmt, args...)    /* Don't do anything in release builds */
#endif

#define STACK_NOITEM 0
#define STACK_ALLOC_FAIL_CNT 3

#ifndef DEBUG_STACK
    #define STACK_MAX 4096
#else
    #define STACK_MAX 1 //must be 2^n
    #define STACK_ALLOC_FAIL_SIM
#endif

typedef struct stack_tt {
    int top;
    int min;
    int max;
#ifdef STACK_ALLOC_FAIL_SIM
    int alloc_cnt;
#endif
    STACK_TTYPE *ptr;
} Stack;

int stack_init(Stack *stack);
int stack_size(Stack *stack);
int stack_top(Stack *stack);
int stack_alloc(Stack *stack);
int stack_realloc(Stack *stack);
void stack_free(Stack *stack);
int stack_push(Stack *stack, STACK_TTYPE item);
int stack_pop(Stack *stack, STACK_TTYPE *item);
void stack_do_all(Stack *stack, int (*fptr)(STACK_TTYPE *, void *), void *params);
void stack_rdo_all(Stack *stack, int (*fptr)(STACK_TTYPE *, void *), void *params);

#endif /* !STACK_HEADER */

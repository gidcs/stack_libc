#include <stdio.h>
#include "stack_type.h"

int stack_print_item(STACK_TTYPE *item, void *params){
    printf("(%d,%d) ", item->first, item->second);
    return 0;
}

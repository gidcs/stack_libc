typedef struct tuple_tt {
    int first;
    int second;
} tuple_t;

#define STACK_TTYPE tuple_t

int stack_print_item(STACK_TTYPE *item, void *params);

